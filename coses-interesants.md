# Coses interessants

## Repositori Kenneth Peiruza

https://github.com/kpeiruza?tab=repositories

## Bloomberg Prometeus (en conjunció amb Kubernetes)

- Sistema de monitorització que substitueix Nagios.

## Selenium

- Primarily, it is for automating web applications for testing purposes, but is certainly not limited to just that. Boring web-based administration tasks can (and should!) be automated as well.

## Concentrador de logs

- Elasticsearch, Logstash, Kibana

- Elastic search és un indexador de fitxers JSON.

- https://www.elastic.co/es/elk-stack
- https://elk-docker.readthedocs.io/
- https://hub.docker.com/r/sebp/elk/

## Almacenamiento distribuido

- Gluster FS:

## Allotjament web

- https://www.online.net/en

## Filtrar continguts

- http://contentfilter.futuragts.com/wiki/doku.php?id=technology_overview

##

pip install youtubedownloader

## HASHTOPOLIS

- http://www.elladodelmal.com/2018/05/hashtopolis-como-crackear-contrasenas.html

## autenticació com LDAP

- freeIPA: autenticació LDAP, que substitueix a OpenLDAP https://www.freeipa.org/page/Downloads#Releases_in_OS_Distributions

## Balanceig fàcil

- Traefic

## Criptografia avançada

- Algorisme ed25519

## BD resistent
- CockroachDB

## preu de serveis al cloud

- Podem calcular costos del servei al núvol a la calculadora d'Amazon.
- https://calculator.s3.amazonaws.com/index.html

- Es pot llogar un botnet DDoS de 300 Gbps durant 1h per 5 euros.
- Amb amplificació "memcached" tenim una amplificació de 1:50000.
- Amb ntp tenim 1:50 (cal buscar el servei per poder enviar les peticions amb ip spoofing per que la resposta vagi a l'atacat o el mateix generi les dades)
- Si l'atacat te serveis al núvol, com que es paga per ús i ample de banda (de sortida o entrada, segons servei cloud), pot costar molts diners (milions d'euros).

## Servei antispam

- https://www.spamhaus.org/

## Obtenir dades de Amazon S3

- Molts llocs estan a internet sense seguretat. Es pot buscar a google com hackejar buckets S3.

## Web possible d'entrar (fer amb Tor)

https://showww.nascom.nasa.gov

## Streaming de presidència del govern
https://webvc.mpr.es

## Gestió maq virt remotes
https://virt-manager.org/

## DRDB

- HA de sistemes d'emmagatzematge
https://en.wikipedia.org/wiki/Distributed_Replicated_Block_Device

## nip.io
- Permet crear registres DNS al lloc nip.iocan be used to load any functions file into the current shell script or a command prompt. per usar-les a la teva xarxa local sense tocar /etc/hosts

~~~
10.0.0.1.nip.io maps to 10.0.0.1
$ ping 10.0.0.1.nip.io
PING 10.0.0.1.nip.io (10.0.0.1) 56(84) bytes of data.

192-168-1-250.nip.io maps to 192.168.1.250
$ ping 192-168-1-250.nip.io
PING 192-168-1-250.nip.io (192.168.1.250) 56(84) bytes of data.
~~~

## Cursos de Google cloud

- qwiklabs: donen saldo per a muntar https://www.qwiklabs.com/?locale=es  

## Avtivitats companys

- https://github.com/joaniznardo/2018smxm7

## Teoremes i característiques bases de dades

CAP theorem: https://en.wikipedia.org/wiki/CAP_theorem
ACID: a las características de los parámetros que permiten clasificar las transacciones de los sistemas de gestión de bases de datos

## Dropbox temporal

- https://transfer.sh


## Cursos del consorci a PUE

- consorci.pue.es

## Hugo static site generator

- https://gohugo.io/getting-started/quick-start/

## Gestió de claus i credencials

- Secure, store and tightly control access to tokens, passwords, certificates, encryption keys for protecting secrets and other sensitive data using a UI, CLI, or HTTP API : https://www.vaultproject.io/

## Proxy invers extern

- https://localxpose.io/
