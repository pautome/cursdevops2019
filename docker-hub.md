# Docker hub i integració amb Github

- Crear compte de Docker hub
- Crear compte de Github
- Crear repositori de Github per ficar els Dockerfile.
  - Pot ser un projecte diferent per cada Dockerfile
  - Pot ser un sol projecte i carpetes per a cada Dockerfile
- Push del que s'ha fet en local
- Enllaçar comptes github i dockerhub
- Configurar enllaç entre projectes (anar al projecte de dockerhub Builds, posar source repositori. Si cal directori a "build rules" escollir directori del repositori github).

- Guia git
- https://rogerdudler.github.io/git-guide/index.es.html

~~~
docker login
pausoft
password
docker push pausoft/imatge-nova:tag

(crear directori per repositori i entrar)
git clone https://github.com/pausoft/docker.git
git add .
git commit
git push
(penja els canvis. Si ha canviat el Dockerfile es desencadena la creació de la imatge si hem marcat l'opció)

~~~

## Gitlab

https://gitlab.com/help/user/packages/container_registry/index.md
