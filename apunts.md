# DEVOPS

- https://docker-docents.pad.floss.cat/p/index

- presentació: https://docs.google.com/presentation/d/1RkufjRnQJDbIumoYYpCkY0i2os1eKhiHHcMmDr-XwXI/edit#slide=id.gc6f73a04f_0_0

## Requisits

- Autonomia
- Auto organització
- Transparencia
- Aut de procesos
- Insfrastructure as code
- Confianza entre ou's
- Minimizar procs y directrices ajenas al equipo
- Doc y archivos de proy accesiblea a todas partes

## Ventajas:

- Ciclos de desarrollo más cortos
  - Mayor freq de implementación
  - Mayor estabilidad (bugfixes)
  - Mayor calidad y seguridad
  - Alineacion con objetivos comerciales

- Menor downtime
- entornos 100 % reproducibles
- Mayor satif para usuarios
- Mayor coop y Confianza (todo transparente)
- Reducc de costes
- Menor sobrecarga admin

## Retroalimentació

- Els desenvolupadors veuen estadístiques i logs. Així poden veure si el que fan va millor.
- Els scripts són visibles.

## Pets vs cattle

- Abans es cuidava el server com si fos una mascota. Ara si va malament, esborres i aixeques un altre com si fos ramat.

- Chat: slack, rocketchat

## Microserveis web

Per integrar serveis entre ells, cada servei programat en un llenguatge

- Rest full API: http URL sense paràmetres i l'objecte a dintre

- Grace Hopper: inventora del clúster. Un computafdor de capacitat x és molt més barat que un de capacitat 2x. Però cal escalar horitzontalment. Cal disociar el servei de l'execució. Exemple: Balancejador amb moltes instàncies darrera que es posen en marxa si fa falta. Estrenar una sèrie a Netflix suposarà alta demanda puntual.

- organització horitzontal: crear Linux o wikipedia costaria molts diners però es va fer gratis. documental "us now"

- L'exercit otomà estava organitzat per directives i gent que pensava independent.

- Analitzar el cas de ING. Va estudiar empreses tecnològiques per reorganitzar-se. Va muntar equips petits (squad i tribes, que agrupen squads amb objectiu comú). Cada membre pot buscar l'squad on encaixi més.

- la mida dels grups de primats és proporcional al nombre de connexions de neurones. 40-120 (Dunbar diu que no podem mantenir relacions amb més de 150 persones). Cal tenir relació directa entre els membres de l'equip, així no tens pensaments "assasins".  

- Documental per veure: the incredible truth about what motivates us
