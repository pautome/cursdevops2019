#!/bin/bash

docker swarm init
docker network create proxy -d overlay
docker network create portainer_agent -d overlay
docker stack deploy -c stack.yaml mistack
