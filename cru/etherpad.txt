#        mon oct  1 16:09:25 CEST 2018
version: '3.4'

services:
  mysql:
    image:  library/mysql:5.7
    networks:
      - default
    environment:
      MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}
    volumes:
      - ${REMOTE_MOUNT}/${STACK_NAME}/mysql:/var/lib/mysql
    deploy:
      replicas: 1
      placement:
        constraints:
          - node.labels.usenode==true

  etherpad:
    image: tvelocity/etherpad-lite
    networks:
      - default
      - proxy
    environment:
      ETHERPAD_TITLE: ${ETHERPAD_TITLE}
      ETHERPAD_ADMIN_PASSWORD: ${ETHERPAD_DB_PASSWORD}
      ETHERPAD_ADMIN_USER: ${ETHERPAD_ADMIN_USER}
      ETHERPAD_DB_TYPE: 'mysql'
      ETHERPAD_DB_HOST: 'mysql'
      ETHERPAD_DB_USER: 'root'
      ETHERPAD_DB_PASSWORD: ${MYSQL_ROOT_PASSWORD}
    depends_on:
      - mysql
    deploy:
      replicas: 1
    deploy:
      labels:
        traefik.port: 9001
        traefik.frontend.rule: "Host:${FQDN}"
        traefik.docker.network: 'proxy'
      placement:
        constraints:
          - node.labels.usenode==true

networks:
  default:
    driver: 'overlay'
  proxy:
    external: true

