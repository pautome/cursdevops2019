# Docker
https://docs.docker.com/get-started/part5/
- Veure un video "devops bigdata kubernetes docker"
- Google trends: Veure tendències (estadístiques de cerca de paraules).

- Logs: datadog i semateg, logs.io

## Instal·lar

~~~
curl https://get.docker.com | sudo bash
~~~

- Instal·lar portainer.io

~~~
docker volume create portainer_data

sudo docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
~~~

- Dintre del contenidor tenim una llista de processos (el bash te el número 1) que tenen números diferents a la màquina host. Es pot buscar el bash des de la màquina host, fer kill i morirà el bash del docker.

- Per a fer pràctiques de xarxes amb contenidors

~~~
--net=host
~~~

- Exemple d'us de Docker:  ffmpeg en contenidor contindrà totes les llibreries i codecs. Així no cal compilar tot que triga molt.

## Comandes Docker

- docker stats: Com el top de Linux amb contenidors.
- docker diff nom-contenidor: Indica diferències entre imatge i contenidor.
- docker exec -ti xavi /bin/bash

- Fent servir filtres del format del fitxer Docker inspect: echo $( docker inspect xavi -f '{{.NetworkSettings.IPAddress}}' )

- docker run -d -p 8080:80 --name xavi wordpress

- Per saber en quin nivell es virtualitza:
- os-level virtualisation

- docker logs -f mysql: Veure logs del contenidor mysql

- docker cp contenidor:/docker-entrypoint.sh .

- docker system-prune: Esborrar tot

---

## Serveis Docker

- Wordpress amb base de dades

~~~
docker run --name mysql -e MYSQL_ROOT_PASSWORD=my-secret -e MYSQL_DATABASE=db_wp -e MYSQL_USER=wp-user -e MYSQL_PASSWORD=p@ssw0rd -d mysql:5.7

docker run -p 8080:80 --link :mysql --name some-wordpress -e WORDPRESS_DB_HOST=10.1.2.3:3306 -e WORDPRESS_DB_USER=wp-user -e WORDPRESS_DB_PASSWORD=p@ssw0rd -d wordpress

docker logs -f mysql: Veure logs del contenidor mysql
~~~

- Per veure script d'inici del contenidor:
- https://github.com/docker-library/wordpress/blob/master/docker-entrypoint.sh

---

## Avís

- Sempre s'ha de posar el tag per indicar versió sobretot en producció. Si no ho posem, pot ser que quan volem construir la imatge no es pugui crear.

- Caldria limitar els tamanys dels logs de docker (un fitxer JSON per cada contenidor) que es troben a /var/lib/docker que és on es troba tot. Per això podem modificar els fitxers de configuració /etc/docker/daemon.json:
- https://docs.docker.com/config/containers/logging/json-file/

- Es pot configurar un local registry amb un contenidor oficial de Docker, però caldrà canviar la configuració del dimoni Docker per a que accepti baixar imatges de llocs no https.

- Es pot indicar quina és la IP per defecte del Docker a l'equip local (172.17.0.1 si no es diu res). Així no tindrem problemes si estem usant aquesta ip per un equip de la xarxa, per exemple.  

Arxiu de config d'exemple de Docker /etc/docker/daemon.json :

~~~    
{
        "dns": [ "8.8.8.8", "8.8.4.4" ],
        "insecure-registries": [ "localhost:5000" ],
        "log-driver": "json-file",
        "log-opts": {
                "max-size": "10m",
                "max-file": "3"
        }
}
~~~

---

## Comprovar xarxa des de dintre el contenidor

- Veurem que es crea un bridge pel contenidor.

~~~
apt install bridge-inetutils
brctl show
sysctl net.ipv4.ip_forward
iptables -nvL
~~~

- Els contenidors amb docker search:
  - Si tenen Official OK vol dir creat per docker
  - Si posa automated OK vol dir que tenen un Dockerfile. Si no, s'ha creat amb un import a mà. Pot ser que hi hagi un binari maliciós.


- El docker afegeix regles de iptables per fer NAT als contenidors. Però posa la política per defecte del FORWARD a DROP i per tant si volem que faci d'enrutador l'equip amb Docker haurem de canviar la pol. per defecte del FORWARD.

---

## Exercicis Docker

- Fer un contenidor que descarregui un youtube i que passi a mp3 en una carpeta mapejada.

~~~
mkdir temazo
vi Dockerfile
instal·la tot el que calgui per tindre operatiu "youtube-dl": ffmpeg

ENTRYPOINT [ "/ruta/a/temazo-modificat.sh"]
....

docker run -v /srv/music:/music temazo thunderstruck seagulls


temazo.sh:

#!/bin/bash
#        https://www.youtube.com/results?search_query=thunderstruck+seagulls

quebusco=$(echo $*  | tr " " "+" )
urlachupar=$(curl -s "https://www.youtube.com/results?search_query=$quebusco"| grep -m1 "/watch" | sed "sx.*href=\"/watchxhttps://www.youtube.com/watchx" | cut -d \" -f1)

FILENAME="$(youtube-dl -x --audio-format mp3 --audio-quality 0 "$urlachupar"  |& fgrep '[ffmpeg] Destination: ' | sed "s/^\[ffmpeg\] Destination: //" )"
echo $FILENAME
NEWFILENAME="$( echo $FILENAME | sed "s/-[^-]\+\.mp3/.mp3/" )"
mv "$FILENAME" "$NEWFILENAME"
mplayer "$NEWFILENAME"
~~~

---

## Entrypoint vs cmd

cmd és la comanda que escrivim darrere la comanda inicial (que ve determinada per ENTRYPOINT)

## Enviar logs a pantalla

- Per a cada fitxer de logs

~~~
ln -s /dev/stdout /var/log/apache2/apache.log
...
~~~

## Docker interessants

https://hub.docker.com/_/haproxy

https://hub.docker.com/r/moodlehq/moodle-php-apache

https://hub.docker.com/r/tomdesinto/maltrail


https://hub.docker.com/r/andrius/asterisk

https://hub.docker.com/r/metasploitframework/metasploit-framework


https://hub.docker.com/r/mysql/mysql-cluster/  . --- oblidar-se  -- kubernetes prometeus operator

https://hub.docker.com/r/f5networks/k8s-bigip-ctlr
https://hub.docker.com/_/mono

https://hub.docker.com/_/wordpressArxiu de config d'exemple de Docker /etc/docker/daemon.json :

~~~
{
        "dns": [ "8.8.8.8", "8.8.4.4" ],
        "insecure-registries": [ "localhost:5000" ],
        "log-driver": "json-file",
        "log-opts": {
                "max-size": "10m",
                "max-file": "3"
        }
}
~~~

https://hub.docker.com/_/microsoft-windows-servercore
https://hub.docker.com/_/oracle-database-enterprise-edition
https://hub.docker.com/r/functions/prometheus
https://hub.docker.com/_/node
https://hub.docker.com/_/nginx

https://hub.docker.com/r/portainer/portainer
https://hub.docker.com/r/beevelop/ionic
https://hub.docker.com/_/amazonlinux


Com creem un Docker "automated build"

Creem un arxiu anomenat "Dockerfile" en un directori "buit".
Des d'aquell directori, executem: docker build . -t ping:maco

~~~

FROM library/debian:stretch-slim
LABEL maintainer="kenneth@floss.cat"

RUN apt-get -y update && \
        apt-get -y install inetutils-ping


ENTRYPOINT [ "/bin/ping", "-c", "3" ]
CMD [ "8.8.8.8" ]

~~~

~~~
FROM library/nginx:latest
MAINTAINER Kenneth Peiruza <kenneth@floss.cat>
RUN rm /usr/share/nginx/html/index.html
ADD web.tgz /usr/share/nginx/html/
~~~


Compte a Github i compte a hub.docker.com


-------------

Dockers macos:

    https://hub.docker.com/r/lthub/moodle/dockerfile   8.0

    https://hub.docker.com/_/mono <---- WTF????????????

    https://hub.docker.com/r/andrius/asterisk/dockerfile 9.5  

    https://hub.docker.com/r/beevelop/ionic tècnicament, tot ok

    https://hub.docker.com/r/lohn/wordpress-ja-fpm-alpine/dockerfile 6 rascat

    https://hub.docker.com/r/mysqludf/ta/dockerfile <---- ojo a la tècnica per a estalviar-se haver de fer el apt-get install en cada reintent de build :-) No ha fet neteja després, però està prou bé

    https://hub.docker.com/r/dockerArxiu de config d'exemple de Docker /etc/docker/daemon.json :

~~~
{
        "dns": [ "8.8.8.8", "8.8.4.4" ],
        "insecure-registries": [ "localhost:5000" ],
        "log-driver": "json-file",
        "log-opts": {
                "max-size": "10m",
                "max-file": "3"
        }
}
~~~

cloud/haproxy/dockerfile sí, mola

    https://hub.docker.com/r/ferrarimarco/pxe/dockerfile 6-7

    https://github.com/neurodebian/dockerfiles/blob/28179a5247bf4681dd56cd11c542f57fafbe0354/dockerfiles/trusty/Dockerfile <--- ESNIFA COLA



Dockers lletjos:

    https://hub.docker.com/r/pingbo/ssh-tunnel/dockerfile Efestiviwonder, l'autor@ sucks

    https://hub.docker.com/r/bjonness406/convert2mkv/dockerfile

    https://hub.docker.com/r/jmatsushita/iris/dockerfile

    https://hub.docker.com/r/ubuntucore/jenkins-ubuntu/dockerfile

    https://hub.docker.com/r/firespring/apache2-php/dockerfile

    https://hub.docker.com/r/zburgermeiszter/traceroute/dockerfile

    https://hub.docker.com/r/openvcloud/pxeboot-init/dockerfile  <-------- aquest NO està malament (potser la imatge que empra de plantilla, sí)

    https://github.com/osrf/docker_images/blob/1a1c56d93f309d10c412c6323db5791fc1b23d1b/ros/indigo/ubuntu/trusty/ros-core/Dockerfile

---

## Volums

~~~
docker volume inspect

docker inspect pepito
~~~

### Drivers

## Xarxes

~~~
docker network create pepe
docker run -ti --network pepe

docker run -it --network pepe --name centollo centos
docker run -it --network pepe --name ubu ubuntu

~~~

~~~
docker run -d -P nginx
(fa assignacio dinàmica de ports)

~~~

## Assignació d'espai extern

Es posa un servidor de dades fora del cluster.

## Balanceig

Tenium una estructura:
1 loadbalancer - X managers - N workers

- La part de lb i managers és estàtica i la de Workers és dinàmica segons les necessitats.
Google no fa clusters de més de 100 màquines. Caldrà posar un lb davant els clusters per fer clusters més grans.

- Mai no muntarem clusters amb un numero parell de nodes (exemple, "estribor qui guanya" i "es babor qui quanya"), tots es pensen que van guanyant.

## Eines

- Per convertir docker run a docker-compose: https://composerize.com/
