# Swarm
https://docs.docker.com/get-started/part5/

## Pasos previs

### PREVIS_fer neteja:

~~~    
root@ubuntu:~# docker system prune
// ESBORRAR contenidors en desús
root@ubuntu:~# docker ps -a | awk '/Exited/ {print $1}' |xargs docker rm

// esborro imatges en desús
root@ubuntu:~# docker images -q | xargs docker rmi

// ESBORRAR VOLUMS EN DESÚS
root@ubuntu:~# docker volume ls -f dangling=true -q |xargs docker volume rm
~~~

### Prerequisits:
  - Les màquines que han de formar part del cluster estan connectades a la mateixa xarxa local.
  - Paquet docker instal·lat en cada màquina.
  - Canviar noms de nodes per saber quins tenim i quin falla: sudo hostnamectl set-hostname jopa02.pue.lan

  ~~~
  sudo hostnamectl set-hostname jopa02.pue.lan
  ~~~

  - Crear storage compartit (per exemple amb NFS) i muntar a tots els nodes. Escriure allà els fitxers yaml dels serveis a desplegar.

## Iniciar cluster (primer manager)

1. Iniciar swarm, automàticament en un dels nodes que ha de tenir el rol de master (vigilar si hi ha més d'una IP al manager. Cal forçar la IP si el docker no la determina correctament):

~~~
docker swarm init [--advertise-addr string]
docker swarm init --advertise-addr 192.168.20.X

Swarm initialized: current node (cc8jq6noerp3bytznbnlzjx0y) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-3sqziog5yq0lb5u6txznafbavwu4gbwxq-19h7cau77njh285x7ci3qwhqe 192.168.20.205:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

// Si volem que s'afegeixi un manager en lloc de worker
docker swarm join-token manager
~~~

2. Visualitzar el token que permet unir al cluster un nou node amb el rol de master/worker:

~~~
docker swarm  join-token (manager | worker)
~~~

El token proposat s'executa en la resta de nodes que han de formar part com a masters o workers del swarm iniciat.

~~~
docker swarm join --token SWMTKN-1-XURRO-DE-TOKEN-QUE-GENERA-EL-PRIMER-MANAGER AQUIPOSALAIP:2377
~~~

- Si cal es pot convertir node worker a manager (només des d'un altre manager) o degradar-lo.

~~~
docker node promote jopa02.pue.lan
docker node promote id, promociona un worker a manager.
docker node demote id, degrada un manager a worker.
~~~

- Des del worker no deixa:

~~~
docker node promote manager
Error response from daemon: This node is not a swarm manager. Worker nodes can't be used to view or modify cluster state. Please run this command on a manager node or promote the current node to a manager.
devops@jopa02:/srv/nfs$  docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
91ih965qpgodkv8i5iq87xth1 *   jopa02.pue.lan      Ready               Active              Reachable           18.09.7
nkzliiu8taz17pmpquil3nlnl     ubuntu              Ready               Active              Leader              18.09.7
~~~

# Des dels que se van a afegir (amb la ip del 1er manager)
token=$(docker -H 172.17.0.88:2345 swarm join-token -q worker) && docker swarm join 172.17.0.88:2377 --token $token

## Afegir Serveis

Les següents ordres només cal executar-les en només un dels nodes master i automàticament la configuració es replica en els altres nodes:

3. Crear les xarxes necessàries en el swarm per connectar els serveis que cal desplegar (els dockers a iniciar). Recordar que al yaml ha d'esmentar les xarxes com :
(al yaml)
...
 networks:
  proxy:
    external: true   
...
Comandes:
docker network create -d overlay portainer_agent
docker network create -d overlay proxy

~~~
$ docker newtwork -d overlay proxy

$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
62f3051fdb66        bridge              bridge              local
1eae6461d869        docker_gwbridge     bridge              local
0ec104b809b6        host                host                local
7rbgquk44t16        ingress             overlay             swarm
4e4a526f7d1a        none                null                local
92qndc8vasb0        proxy               overlay             swarm
~~~

4. Desplegar els stacks en el swarm:
docker stack deploy -c stack-portainer.yaml portainer
docker stack deploy -c stack-traefik.yaml traefik

5. Per forçar que una màquina deixi el swarm creat:
docker swarm leave --force

## Comprovar estat del swarm

~~~
docker service ls
docker node ls, per llistar els nodes del swarm, (l'asterisc al costat de l'indentificador indica el node actual, des del qual s'ha executat l'ordre).
docker network ls
~~~

## En general

- Definim un servei i després podrem escalar creant a cada node els contenidors que calguin. Crear fitxer yaml com si fos del docker-compose (exemple redis del docker-compose).
- A kubernetes podem lligar el creixement a mètriques (per exemple, servei carregat al 40% engega un nou container).
- Tots els nodes del cluster compartiran simultàniament el port exposat del servei que sigui, és a dir, als nodes es creen contenidors amb els serveis i els mateixos ports oberts.
- Les sessions que s'obren amb per exemple PHP es guardarien a cada node i això faria que en canviar de node demanaria de nou iniciar la sessió. Cal preservar-les:
  - En un primer intent, NFS amb carpetes comunes. Massa lent. Un altre, BD SQL. També lent.
  - Memcache_d permet accedir és un key value store.
  - redis: (millor solució) Tenim un servidor redis únic (o muntat en cluster) una BD noSQL que permet guardar les sessions.
- Llavors, normalment es fa servir "redis" (in-memory data structure store, used as a database, cache and message broker) amb contenidors darrera per a augmentar la capacitat.

## Serveis interessants a desplegar (alguns stacks)

- Portainer.io: Manager web per docker i swarm.
- Stack TIG (Telegraf, InfluxDB y Grafana):
    - Telegraf: recolectar totes les dades de logs (CPU/RAM/LOAD o serveis com Nginx, MariaDB, etc.)
    - InfluxDB: On Telegraf envia aquesta informació. InfluxDB disenyat per guardar de manera eficient una quantitat important d'informació. Es pot definir periodes de retenció de la informació en cas que hi hagi problemes de rendiment.
    - Grafana: Dashboard que mostra tota la información que InfluxDB te a les BD.
    https://www.solvetic.com/tutoriales/article/6396-como-instalar-telegraf-influxdb-y-grafana-tig-stack-en-ubuntu-linux/
- Stack Prometheus+grafana: Stack alternatiu que millora el TIG. https://medium.com/@salohyprivat/prometheus-and-grafana-d59f3b1ded8b
- Traefik: Proxy invers per a microserveis. Posar en marxa en un node manager per reenviar les peticions que li arribin des de fora cap al microservei que toqui, que pot viure a qualsevol node worker (millor no al manager). Gestiona automàticament els certificats Let's Encrypt (els anomena "acme"). https://docs.traefik.io/
- Stack ELK (Elasticsearch, Logstash, and Kibana):
    - Elasticsearch is a search and analytics engine.
    - Logstash is a server‑side data processing pipeline that ingests data from multiple sources simultaneously, transforms it, and then sends it to a "stash" like Elasticsearch.
    - Kibana lets users visualize data with charts and graphs in Elasticsearch.

- https://github.com/kpeiruza/docker-stacks/tree/master/logspout-elk


## Exercicis

- Desplegament Swarm
  - Cluster 1 master 2 workers ?
  - Desplegar concentració de mètriques (TIG)
  - Concentrar logs
  ...

- Exemples de provar serveis escalats (posar en marxa i comprovar que hi ha els dockers engegats):

~~~
docker service scale wredi_web=30

docker ps
docker stats

ps -ef | grep python | grep flask
kill per matar els Serveis
~~~

- Swarm s'adona que els has matat i els reinicia

~~~
version: '3'
services:
  traefik:
    image: library/traefik:1.7
    command:
      - "--api"  
      - "--entrypoints=Name:http Address::80"
      - "--entrypoints=Name:https Address::443 TLS"
      - "--defaultentrypoints=http,https"
      - "--docker"
      - "--docker.swarmmode"
      - "--docker.domain=floss.cat"
      - "--docker.watch"
# Traiem Redirect del 80 al 443 per fer proves:  Redirect.EntryPoint:https
#      - "--acme"
#      - "--acme.entryPoint=https"
#      - "--acme.httpChallenge.entryPoint=http"
#      - "--acme.OnHostRule=true"
#      - "--acme.onDemand=false"
#      - "--acme.email=ELTEU@EMAIL.CAT"
#      - "--acme.storage=/etc/traefik/acme/acme.json"
#      - "--logLevel=DEBUG"
#      - "--traefikLog.filePath=/dev/sterr"
#      - "--accessLog.filePath=/dev/stdout"
#      - "--accessLog.filters.statusCodes=200,300-302,400-460,500"
#      - "--accessLog.filters.retryAttempts=true"
#      - "--accessLog.fields.defaultMode=keep"
#      - "--accessLog.fields.names=Username=drop"
#      - "--accessLog.fields.headers.defaultMode=keep"
#      - "--accessLog.fields.headers.names=User-Agent=redact Authorization=drop Content-Type=keep"
#      - "--traefikLog.format=json"
#      - "--accessLog.format=json"
    networks:
      - proxy
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /srv/nfs/traefik/acme:/etc/traefik/acme/
    ports:
      - 80:80
      - 443:443
      - 8080:8080
    deploy:
      restart_policy:
        condition: on-failure
      placement:
        constraints:
          [ node.role == manager ]

networks:
  proxy:
    external: true

cia.
~~~

- Matar el cluster
~~~
docker stack rm
docker ps
~~~

- Treballant en cluster les variables d'entorn no es poden passar de màquina a màquina i per tant cal duplicar la definició.

~~~
docker stack services docker-docents
docker service inspect docker-docents
~~~

- Cal posar sempre etiquetes (tags) per poder després buscar els objectes (contenidors, xarxes, etc) sense haver de buscar per IP's o fer inventari.

- Hi ha un volum especial que en realitat és un socket que permet al Traefik i al portainer obtenir les dades dels contenidors i fer la seva feina.

~~~
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
~~~

---

## Esquema de la infraestructura de serveis:

- Darrera el router internet, posar un Load Balancer que rebi les peticions. Si aquest LB pot estar en HD millor (cal fer storage compartit i posar failover o actiu-actiu). També hauriem de tenir dues línies d'internet en proveïdors diferents per que no caigui la línia i que les dues vagin amb IP única. Podem contractar packs de IP's fixes a un proveïdor extern https://www.ovh.es/private-cloud/opciones/direcciones-ip.xml i posar un host que reenvii el tràfic cap a l'entrada que volem (balanceig des de un host d'internet). Caldrà veure com ho fem, una manera és configurant VPN entre el node d'internet i cada línia d'entrada... És un pollo!!
- Darrera aquest LB tenim els nodes manager amb Traefik, que reenviaran les peticions als microserveis que es troben a la següent línia de nodes worker.
- Darrera els managers tenim els nodes worker amb contenidors que fan el treball (WP, BD, etc)
- Tots els nodes del cluster swarm tenen espai compartit per ficar els fitxers yaml que defineixen els serveis i els volums per fer la persistència de dades.
- Seria interessant fer que l'storage tingués HA per exemple amb drbd tot i que és un sistema costós.  

## Pràctiques amb alumnes:

  - 1 LB (en HA posant el filesystem en NFS i fent que sigui virtual)
  - 3 nodes de clusters
  - 1 NFS (HA amb drbd)

## Plantilles clusters (es veuen al portainer quan s'instal·la)

- portainer.io templates

## Gestor de màquines virtuals remotes

- https://virt-manager.org/

## Crear Kubernetes en baremetal
- Tectonic
- https://www.youtube.com/watch?v=61tSi50rfRI
- https://tectonic.com/docs/
