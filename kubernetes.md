# kubernetes

## Xarxa del docker per no tenir problemes amb Kubernetes i la xarxa de l'institut

calico: fa servir xarxa 192.168.0.0/16
weave: xarxa 10.0.0.0/24

## Monitorització:

- Passem del nagios que triga molt en avisar.

## TICK Stack
- Telegraf
- influxdb
- Capacitor
- K

## Prometeus

- Monitorització amb el seu format propi.
- Visualització amb Grafana.


pod security policies de kubernetes per poder assegurar que un contenidor funciona com sempre i no ens han ficat malware (congela per inspecció i arrenca un altre contenidor).

## Minikube

- Manera més ràpida de tenir Kubernetes
- Kubespray requereix uns entorns especials.
- Katakoda: Formació tecnologica via web. Per a fer proves en laboratoris. Kubernetes: https://www.katacoda.com/courses/kubernetes (fer els cinc primers apartats).


## Conceptes kubernetes

### Arquitectura

- En swarm tenim un dimoni a cada màquina i un storage compartit.
- Per kubernetes és més complex. Tenim una arquitectura especial.

- Pod: Unitat mínima de desplegament. Normalment un contenidor. En realitat un conjunt de contenidors que comparteixen xarxa i storage i separat en un node.

- etcd: key value store per a cluster. Per guardar per exemple info de sessions.

- tenim un resfull API que gesiona la comunicació amb el Kubernetes. Li pots passar JSON o yml (que són equivalents).

- Model bàsic un master i n workers (nodes o minions). Amb alta disponibilitat múltiples masters.

- Deployment: Definició d'un tipus de servei. Quan el llences i l'atures guarda als logs tots els desplegaments.

- En swarm podem usar HEALTHCHECK. A Kubernetes tenim readiness check i HEALTHCHECK.
- Pots crear volums però en el seu lloc es fa "persistent volume claim" (objecte de kubernetes) per persistir dades, que provisionen volums on demand. Tipus: RWO (rw once), RWX, ROO (ro once).

- Cal definir els drivers de xarxa i storage a Kubernetes. A swarm teniem el driver genèric overlay (molt més fàcil).

- https://medium.com/@geraldcroes/kubernetes-traefik-101-when-simplicity-matters-957eeede2cf8

## helm

- Eina de Microsoft per a crear serveis de kubernetes. te plantilles de tots els yaml que cal crear (per definir serveis, pods, etc)

## Prometheus
- https://medium.com/faun/trying-prometheus-operator-with-helm-minikube-b617a2dccfa3
