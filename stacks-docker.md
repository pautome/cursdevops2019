# Llistat de Stacks desplegats:

  * Traefik:  pujar a GIT el del PAD!!!!!!
  * Portainer: https://github.com/kpeiruza/docker-stacks portainer
  * TIG Stack (o millor encara, prometheus+grafana!!!!): buscar URL de l'Òscar: https://github.com/lopsoros/tig-stack
  * ELK <---- devorador de recursos: https://github.com/kpeiruza/docker-stacks logspout-elk

## Descripció

- Portainer.io: Manager web per docker i swarm. https://github.com/kpeiruza/docker-stacks portainer

- Stack TIG (Telegraf, InfluxDB y Grafana):
  - Telegraf: recolectar totes les dades de logs (CPU/RAM/LOAD o serveis com Nginx, MariaDB, etc.)
  - InfluxDB: On Telegraf envia aquesta informació. InfluxDB disenyat per guardar de manera eficient una quantitat important d'informació. Es pot definir periodes de retenció de la informació en cas que hi hagi problemes de rendiment.
  - Grafana: Dashboard que mostra tota la información que InfluxDB te a les BD.
  https://www.solvetic.com/tutoriales/article/6396-como-instalar-telegraf-influxdb-y-grafana-tig-stack-en-ubuntu-linux/

- Traefik: Proxy invers per a microserveis. https://docs.traefik.io/

- Stack ELK (Elasticsearch, Logstash, and Kibana):
  - Elasticsearch is a search and analytics engine.
  - Logstash is a server‑side data processing pipeline that ingests data from multiple sources simultaneously, transforms it, and then sends it to a "stash" like Elasticsearch.
  - Kibana lets users visualize data with charts and graphs in Elasticsearch.

- https://github.com/kpeiruza/docker-stacks/tree/master/logspout-elk
